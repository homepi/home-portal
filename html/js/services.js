
// ## config
var prev = [];

// ## fund
var get_apps = async function(){
	//console.log('get_apps - start...');
	
	var data = await getPromise(SVC.hub + "/api/apps");
	
	if( data.length !== prev.length ){
		console.log('get_apps - body = %j', data);
		$('#svc_list > tbody').html('');
		
		//var row = trow('name', 'kind', 'url');
		//$('#svc_list > tbody').append(row);
		
		data.forEach(function(x){
			
			var group = x.group || '';
			
			var kind = x.kind;
			if( kind === 'docker' ){
				kind = `<a href="${x.port_url}" target="_blank" >${x.kind}-${group}</a>`;
			}
			
			var anch = x.url || '';
			var shouldHyper = (
				x.url && x.url.length
				&& (
					(x.labels && x.labels.tier === 'app')
					|| x.url.includes('1880') || x.url.includes('9880')
					|| x.url.includes('9000')
					|| x.url.includes('9002')
					|| x.url.includes('3001')
					|| x.url.includes('10060') || x.url.includes('10058')
					|| x.url.includes('9004')
				)
			);
			if( shouldHyper ){
				anch = `<a href="${x.url}" >${x.url}</a>`;
			}
			
			
			var remote = "";
			if( x.remote ){
				remote = `<a href="${x.remote}" >${x.remote}</a>`;
			}
			
			var my_row = trow(x.name, kind, anch, remote);
			$('#svc_list > tbody').append(my_row);
		});
		
		prev = data;
	}
};

run_loop(get_apps, 10000);


