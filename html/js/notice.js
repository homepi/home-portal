
var Global_Notice_List = [];
var NOTICE_MAX = 5;

var get_notice = async function(){
	
	var data = await getPromise(SVC.hub + "/api/db/portal/notice_new");
	console.log("get_notice - data: " + JSON.stringify(data));
	$("#notice_holder").empty(); // Clean notes
	
	if( data.length === 0 ){
		$("#alert_bell").css({ color: '' });
		return false;
	}
	
	data = data.slice(-NOTICE_MAX);
	//Compare if New came
	var isChanged = (Global_Notice_List.length > 0) ? (Global_Notice_List[0].time !== data[0].time) : true;
	/*
	console.log("get_notice - global.length: " + Global_Notice_List.length.toString());
	console.log("get_notice - isChanged: " + isChanged.toString());
	if(Global_Notice_List[0]){
		console.log("get_notice - old.time: " + Global_Notice_List[0].time);
		console.log("get_notice - new.time: " + data[0].time);
	}
	*/
	// TODO Improve IsChanged E.g. First entry removed, but no real NEW entries, thus 
	// Check to be for all Data check if not yet present in Global (array.some?)

	if(isChanged){
		//$("#alert_bell").css({ color: '#EC6161' });
		console.log("get_notice - isChanged: " + isChanged.toString());
	}
	
	Global_Notice_List = data;
	//Populate holder
	data.forEach(function(x){
		$("#notice_holder").append('<li data-id="' + x.id + '"><a href="#">' + x.msg + '</a></li>');
	});
	
	//$("#notice_holder").append('<li class="divider"></li>');
	// TODO Append at the end <li><a href="#">View All</a></li>
	
	/* 
	 * TODO IF Notify API permission Granted
	 * THEN PUSH First Notification only
	 */
	
};

function notice_ack(id){
	console.log("notice - ACK: " + id);
	// TODO Implement notice Removal request
}

function bell_click(){
	console.log("notice - Bell click: " + id);
	// TODO Implement notice Removal request
}

run_loop(get_notice, 10000);


