
// ## config


// ## func
var render_space = function(target, obj, label){
	
	var used = parseFloat(obj.used);
	var p_used = parseFloat(obj.p);
	var units = "MB";
	if( used > 1024 ){
		used = Math.floor(used / 1024);
		units = "GB";
	}
	var txt = used + ' ' + units;
	
	$(target).text(label + " = " + txt);
	
	if( p_used < 20 ){
		$(target).css({ width: '20%' });
	}
	else {
		$(target).css({ width: obj.p });
	}
	
};

function get_pi_status(){
	
	var CPU_TEMP_MAX = 80;
	var PI_STATUS_URL = 'http://' + SVC.hub + '/api/status';
	
	//console.log("myjs.get_pi_status - start...");
	
	var callbackGood = function(data){
		console.log("myjs.get_pi_status - data: %j", data);
		/* {total: 2, rows: [{id:"hub"}, {}]} */
		
		data.rows.forEach(function(obj){
			
			var x = obj.id;
			
			// CPU-temp
			if( obj.cputemp ){
				var target = "#temp_" + x;
				$(target).text( obj.cputemp.toString() + " C" );
				var isHighTemp = obj.cputemp >= (CPU_TEMP_MAX * 0.8);
				var temp_p = Math.round(100 * obj.cputemp / CPU_TEMP_MAX);
				
				if(isHighTemp){
					$(target).addClass("progress-bar-danger");
				} else {
					$(target).removeClass("progress-bar-danger");
				}
				
				$(target).css({ width: (temp_p.toString() + "%") });
			}
			
			// RAM
			target = "#ram_" + x;
			
			var ram_text = Math.round(obj.ram.used / 1024).toString();
			if( obj.ram.unit === 'm' ){
				ram_text = obj.ram.used.toString();
			}
			
			$(target).text(ram_text + " MB");
			$(target).css({ width: (obj.ram.p) });
			
			// Disk
			for(var d in obj.disk){
				target = ["#space", x, d].join('_');
				render_space(target, obj.disk[d], d);
			}
			
		});
		
	};
	
	$.ajax({
		type: 'GET',
		url: PI_STATUS_URL,
		dataType: 'json',
		success: callbackGood,
		error: function(err) {
			console.log("myjs.get_pi_status - err: " + JSON.stringify(err));
		}
	});
	
}

function get_indoor(){
	
	var TEMP_MAX = 40;
	var HUM_MAX = 60;
	var PI_TEMP_URL = 'http://' + SVC.hub + '/api/temp';
	var elem = $('#panelBodyIndoor pre');
	
	var callbackGood = function(result){
		
		var oneRoom = result.rows[0];
		
		var tempRatio = Math.round(oneRoom.cal / TEMP_MAX * 100);
		$('#indoor_temp').text( oneRoom.cal + " C");
		$('#indoor_temp').css({ width: (tempRatio + '%') });
		
		var humRatio = Math.round(oneRoom.humidity / HUM_MAX * 100);
		$('#indoor_hum').text( oneRoom.humidity );
		$('#indoor_hum').css({ width: (humRatio + '%') });
		
		///// ------
		oneRoom.temperature = oneRoom.cal;
		delete oneRoom.cal;
		
		var fmtText = '';
		Object.keys(oneRoom).forEach(function(p){
			fmtText += `${p} = ${oneRoom[p]}`;
			fmtText += "\n";
		});
		
		elem.html(fmtText);
	};
	
	$.ajax({
		type: 'GET',
		url: PI_TEMP_URL,
		dataType: 'json',
		success: callbackGood,
		error: function(err) {
			console.log("myjs.get_indoor - err: " + JSON.stringify(err));
		}
	});
	
};

function main(){
	//console.log("myjs - Started");
	
	run_loop(get_pi_status, 5000);
	run_loop(get_indoor, 5000);
	
}

main();


