
// ## config
var chart = null;

// ## fund
var get_devices = async function(){
	
	//console.log('get_devices - start...');
	var data = await getPromise(SVC.hub + "/api/sys");
	//console.log('get_devices - result = %j', data);
	
	//$('#test').html(JSON.stringify(data, null, 2));
	$('#sys-tbody').html('');
	
	data.rows.forEach(function(s){
		var row = trow(s.id, s.update, s.os, s.ts);
		$('#sys-tbody').append(row);
	});
	
};

var get_ram = async function(){
	
	var data = await getPromise(SVC.hub + "/api/cache/list:ram-history?end=100");
	
	//var filt = data.filter(x => x.id === 'hub').reverse();
	var reversed = data.reverse();
	var keys = [];
	
	var mapped = reversed.map( x => {
		
		var date = new Date( x.time );
		date.setMilliseconds(0);
		
		var divider = 30;
		var everyFive = Math.round(date.getSeconds() / divider) * divider; // 16 / 5 => 3 * 5 => 15
		
		if( everyFive > 59 ){
			date.setSeconds(0);
			date.setMinutes(date.getMinutes() + 1);
		}
		if( everyFive < 1 ){
			date.setSeconds(0);
			date.setMinutes(date.getMinutes() - 1);
		}
		if( everyFive >= 1 &&  everyFive <= 59 ){
			date.setSeconds(everyFive);
		}
		
		var obj = {
			time: date.toISOString().substring(0,19)
		};
		var key = x.id;
		var val = x.ram.p;
		obj[key] = val;
		
		if( !keys.includes(key) ){
			keys.push(key);
		}
		
		return obj;
	});
	
	var joined = [];
	mapped.forEach(function(x){
		
		var found = joined.find(y => y.time === x.time);
		if( found ){
			Object.assign(found, x);
		}
		else {
			joined.push(x);
		}
	});
	
	keys.sort();
	if( chart ){
		chart.setData(mapped);
	}
	else {
		chart = new Morris.Line({
			element: 'ram-hub',
			data: joined,
			xkey: 'time',
			ykeys: keys,
			labels: keys,
			pointSize: 0,
			hideHover: true
		});
	}
};

var get_time_unit = function(obj, first, sec, num){
	if( obj[first] > num ){
		obj[sec] = Math.floor(obj[first] / num);
		obj[first] = obj[first] % num;
	}
};

var get_jobs = async function(){
	var data = await getPromise(SVC.esb + "/jobs");
	var now = new Date();
	
	data.rows = data.rows.map(function(x){
		
		var end = new Date(x.end);
		
		var diff = Math.round((now - end) / 1000);
		var ago = {
			sec: diff,
			min: 0,
			hr: 0,
			d: 0
		};
		
		get_time_unit(ago, 'sec', 'min', 60);
		get_time_unit(ago, 'min', 'hr', 60);
		get_time_unit(ago, 'hr', 'd', 24);
		
		x.ago = `${ago.d}d - ${ago.hr}:${ago.min}`;
		return x;
	});
	
	$("#jobs").html( JSON.stringify(data, null, 2) );
};

run_loop(get_devices, 5000);
run_loop(get_ram, 5000);
run_loop(get_jobs, 5000);

